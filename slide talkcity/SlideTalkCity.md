---
marp: true
theme: cedup
style: |
    header, footer {
    }
    section.lead {
        background: linear-gradient(#222, #292929);
    }
    section{
        background-image: url("slidebackground.png");
    }
    h6 {
    font-size: 0.7em;
    }
    blockquote {
        font-size: 0.7em;
    }
    th {
        background-color: #333;
        color: #FFF;
    }

class:
  - lead
  - invert
---

<!-- 
header: Centro de Educação Profissional Abílio Paulo
footer: Projeto Integrado de Desenvolvimento do Curso de Ensino Médio Integrado em Informática 
-->

![bg right:35%](img/tela2.png)

# TalkCity
## Até porque você também tem o direito de falar!

Amanda Nascimento Schwanck
Maria Theresa Fernandes Philippi

###### *Criciúma, Novembro de 2019*

---

<!--
paginate: true
class:
-->

## Índice

1. Introdução e Justificativa
1. Projeto de Software
    1. Análise de Requisitos
    1. Modelagem de Dados
1. Software Desenvolvido
    1. Linguagens e Persistência de Dados
    1. Frameworks, Ferramentas e Recursos
1. Demonstração
1. Conclusão e Agradecimentos

---

## Introdução

![bg right:50% fit](img/tcctela.png)


* Ideia inicial;
* Software;
* Envolvimento social;
* Área de aplicação.

---

## Justificativa

![bg right:50% fit](img/tcctela.png)

* Democracia;
* Jean-Jacques Rousseau;
* Era de informação e tecnologia;
* Entrevista.

---

## Entrevista

Pergunta 1

> * “Sempre. Se existe um sistema com essa finalidade eu desconheço. Seria muito útil”.
> * "Sim, mas tenho que chegar até a central de atendimento. Mas algumas pessoas não podem se dirigir e persiste o problema."

Pergunta 2

> * “Usaria. Acredito que se tivesse esse tipo de sistema no nosso bairro, talvez a voz da comunidade fosse ouvida. Já que o descaso da gestão com os moradores é imenso”.
> * "Com certeza. Deveria sim ter um espaço oficial para expor nossas ideias e nossos problemas, com certeza, e isso deveria estar garantido como forma de lei oficializada."

---

## Projeto de Software

![bg right:50% fit](img/tcctela.png)

---

## Análise de Requisitos

### Requisitos Funcionais

| Requisito | Descrição |
| -- | -- |
| Cadastro de usuários | Cadastra os usuários no banco de dados. |
| Registros de reclamações, dúvidas e sugestões. | Insere em suas devidas tabelas os registros fornecidos pelos usuários. |
| Publicação de reclamações, dúvidas e sugestões. | Mostra os dados fornecidos pelo banco na tela. |
| Notificações | Emite notificações conforme há as atualizações da prefeitura. |

---


## Análise de Requisitos

### Requisitos Funcionais

| Requisito | Descrição |
| -- | -- |
| Login | Autenticação de usuários por nome e senha. |
| Bate-papo | Recebe e envia mensagens em caixas de texto (como Messenger). |
| Ideias | Mostra ideias classificadas por categorias. |

---

## Análise de Requisitos

### Requisitos Não-Funcionais

| Requisito | Descrição |
| -- | -- |
| Banco de Dados | Conexão entre o protótipo e o Firebase. |
| Dúvidas | Serão recebidas apenas pela prefeitura e quando respondidas, serão mostradas ao usuário. |
| Ferramentas | O sistema será desenvolvido em Kodular, Firebase e programação em bloco. |

---

## Modelagem de Dados

![bg right:50% fit](img/tcctela.png)

---

# Diagrama de Caso de Uso

---

![bg right: fit](img/casouso.png)

---

# Diagrama de Classe

---

![bg left: fit](img/classe.png)

---

## Software Desenvolvido

![bg right:50% fit](img/tcctela.png)

---

### Linguagens Usadas

![bg left:30% fit](img/kodular.png)

* Kodular Creator -  Programação em blocos 

---

### Persistência de Dados

![bg left:30% fit](img/banco.png)

* Firebase Console

---

### Frameworks e Ferramentas

![bg left:30% fit](img/kodular.png)
![bg left:30% fit](img/gitlab.png)
![bg left:30% fit](img/canva.png)

* Kodular Companion 
* GitLab
* Canva 

---

### Recursos Utilizados

![bg right:30%](img/tela2.png)

* Computador (Intel Cerelon N2840 2.166GHz, 4,00 GB de RAM, 500GB de espaço de armazenamento);
* Smartphone (Motorola One);
* Uma conta com acesso ao Google Firebase;
* Uma conta com acesso ao Kodular Creator.

---

# Demonstração

---

## Conclusões

![bg right:30%](img/tela2.png)

* Aprimoramento do raciocínio lógico;
* Uso e conexão de banco de dados não-relacional;
* Aprendizagem de ferramentas nunca vistas em sala de aula;
* Tempo.
---

## Projetos Futuros

![bg right:30%](img/tela2.png)

* Notificações de mensagens;
* Perfil dos usuários;
* Criação do perfil da prefeitura contendo todos os atendimentos realizados;
* Notificação de eventos locais;
* Mapa com os serviços públicos.
---

## Agradecimentos

![bg right:40% fit](img/aluno1.jpg)
![bg vertical fit](img/aluno2.jpg)

>Agradecemos aos professores e familiares que colaboraram com o trabalho.

---

![bg left:30% fit](img/tela2.png)

# Obrigado pela atenção!
## Dúvidas?

Amanda Nascimento Schwanck 
Maria Theresa Fernandes Philippi

> *Todas as imagens usadas nesta apresentação são de domínio público, dos autores do trabalho ou propriedade de suas respectivas marcas*